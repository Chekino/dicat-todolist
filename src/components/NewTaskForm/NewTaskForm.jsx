import React from "react";
import { useState } from "react";
import "./newtaskform.css";
import { toast } from "sonner";
import { BsPencilSquare } from "react-icons/bs";

const NewTaskForm = ({ onAddTask }) => {
  const [title, setTitle] = useState("");

  function handleSubmit(e) {
    e.preventDefault();
    onAddTask(title);
    setTitle("");
    toast.success("Tache crée avec succès");
  }

  function onChangeTitle(e) {
    setTitle(e.target.value);
  }
  return (
    <div className="justify-content-center d-flex align-items-center ">
      <form onSubmit={handleSubmit} className="newTaskForm ">
        <input
          type="text"
          placeholder="Ajouter une tâche"
          value={title}
          onChange={onChangeTitle}
        />
        <button>
          Créer <BsPencilSquare size={22} />
        </button>
      </form>
    </div>
  );
};

export default NewTaskForm;
