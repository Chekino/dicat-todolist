import React from "react";
import "./task.css";
import { toast } from "sonner";
import { IoTrashSharp } from "react-icons/io5";
import { AiFillCheckCircle } from "react-icons/ai";
const Task = ({ task, onComplete, onDelete }) => {
  function handleDelete() {
    onDelete(task.id);
    toast.error("Tache supprimé avec succès");
  }

  const handleCompleteClick = () => {
    if (task.isCompleted) {
      toast.error("Tâche incomplète");
    } else {
      toast.success("Tâche complétée ");
    }
    onComplete(task.id);
  };
  return (
    <div className="task">
      <button
        className="checkContainer"
        onClick={() => {
          handleCompleteClick();
        }}
      >
        {task.isCompleted ? <AiFillCheckCircle size={22} /> : <div />}
      </button>
      <p className={task.isCompleted ? "textCompleted" : ""}>{task.title}</p>
      <button className="deleteButton" onClick={handleDelete}>
        <IoTrashSharp size={22} />
      </button>
    </div>
  );
};

export default Task;
