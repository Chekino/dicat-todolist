import React from "react";
import "./newtask.css";
import Task from "../Task/Task";

const NewTask = ({ newTask, onComplete, onDelete }) => {
  const tasksQuantity = newTask.length;
  const completedTasks = newTask.filter((task) => task.isCompleted).length;
  return (
    <section className="new-tasks">
      <header className="new-task__header">
        <div>
          <p>Taches Créer</p>
          <span>{tasksQuantity}</span>
        </div>
        <div>
          <p className="textPurple ">Taches completée</p>
          <span>
            {completedTasks} of {tasksQuantity}
          </span>
        </div>
      </header>
      {tasksQuantity === 0 ? (
        <p className="aucune-tache">Aucune tâche disponible pour l'instant</p>
      ) : (
        <div className="list">
          {newTask.map((task) => (
            <Task
              key={task.id}
              task={task}
              onComplete={onComplete}
              onDelete={onDelete}
            />
          ))}
        </div>
      )}
    </section>
  );
};

export default NewTask;
