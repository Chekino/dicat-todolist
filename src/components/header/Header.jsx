import React from "react";
import { useState, useEffect } from "react";
import "./header.css";
import { PiHandsClappingFill } from "react-icons/pi";
const Header = () => {
  const texts = [
    "Quels sont vos plants du jours ?",
    "Exploitez le pouvoir de la productivité!",
    "Restez organisé, gardez une longueur d'avance.",
  ];

  const [indexActuel, setIndexActuel] = useState(0);
  useEffect(() => {
    const interval = setInterval(() => {
      setIndexActuel((prevIndex) => (prevIndex + 1) % texts.length);
    }, 3000);
    return () => clearInterval(interval);
  }, []);
  return (
    <div className="container py-3">
      <div className="row">
        <div className="col-6 text-nowrap">
          <span className="logocolor">
            <PiHandsClappingFill size={50} />
          </span>
          <p className="">Bonjour et Bienvenue</p>
        </div>
        <div className="row">
          <div className="col-6 text-nowrap">
            {texts.map((text, idx) => (
              <div
                key={idx}
                style={{ display: idx === indexActuel ? "block" : "none" }}
                className="small-text"
              >
                {text}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
