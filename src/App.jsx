import NewTask from "./components/NewTask/NewTask";
import NewTaskForm from "./components/NewTaskForm/NewTaskForm";
import Header from "./components/header/Header";
import { useEffect, useState } from "react";

const LOCAL_STORAGE_KEY = "todo:savedTasks";
function App() {
  const [tasks, setTasks] = useState([]);

  function loadSavedTasks() {
    const savedTasks = localStorage.getItem(LOCAL_STORAGE_KEY);
    if (savedTasks) {
      setTasks(JSON.parse(savedTasks));
    }
  }

  useEffect(() => {
    loadSavedTasks();
  }, []);
  function setTasksAndSave(newTasks) {
    setTasks(newTasks);
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(newTasks));
  }

  function AddTask(task) {
    setTasksAndSave([
      ...tasks,
      { id: Date.now(), title: task, isCompleted: false },
    ]);
  }
  function deleteTaskById(taskId) {
    const newTasks = tasks.filter((task) => task.id !== taskId);
    setTasksAndSave(newTasks);
  }
  function toggleTaskCompletedById(taskId) {
    const newTasks = tasks.map((task) => {
      if (task.id === taskId) {
        return { ...task, isCompleted: !task.isCompleted };
      }
      return task;
    });
    setTasksAndSave(newTasks);
  }

  return (
    <>
      <Header />
      <NewTaskForm onAddTask={AddTask} />
      <NewTask
        newTask={tasks}
        onComplete={toggleTaskCompletedById}
        onDelete={deleteTaskById}
      />
    </>
  );
}

export default App;
